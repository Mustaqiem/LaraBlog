<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/like/{article}', 'LikeArticleController@setLikeArticle');
Route::get('/unlike/{article}', 'LikeArticleController@setUnlikeArticle');
Route::get('/like/count/{article}', 'LikeArticleController@countLikeArticle');

Route::get('/like/comment/{id}', 'CommentArticleController@setLikeComment')->name('like-comment');

Route::post('/comment/{article}', 'CommentArticleController@createCommentArticle')->name('comment.article');
Route::get('/comment/all/{article}', 'CommentArticleController@getAllCommentArticleById');
// Route::get('/comment/count/{article}', 'LikeArticleController@countLikeArticle');
// Route::get('/comment/count/{article}', 'LikeArticleController@countLikeArticle');


Route::get('/profile', 'ProfileController@profile');

Route::get('/profile/change-image', 'ProfileController@getChangeImage');
Route::post('/profile/change-image', 'ProfileController@postChangeImage')->name('change-image');
Route::get('/profile/change-password', 'UsersController@getChangePassword');
Route::post('/profile/change-password', 'UsersController@changePassword')->name('change-password');


Route::resource('articles', 'ArticlesController');

