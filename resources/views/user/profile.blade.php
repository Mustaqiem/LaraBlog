@extends('layouts.app')

<style>
    .profile-img {
        max-width: 150px;
        border: 5px solid #fff;
        border-radius: 100%;
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3)
    }
    
    div.row{
        width:100%;
    }
</style>

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
            <img src="{{ Storage::url(Auth::user()->photo) }}" alt="" class="profile-img">
            <h1> {{ Auth::user()->name }} </h1>
            <h5> {{ Auth::user()->email }} </h5>
            <h5> {{ Auth::user()->dob->format('l j F Y') }} ({{ Auth::user()->dob->age }} years old) </h5>

            <a href="/profile/change-image"><button class="btn btn-success">Change photo</button></a> 
            <a href="/profile/change-password"><button class="btn btn-success">Change password</button></a> 
            <a href="/profile/change-password"><button class="btn btn-success">Update Profile</button></a> 
            </div> 
        </div>
    </div> 
</div> 
@endsection