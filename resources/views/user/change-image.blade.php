@extends('layouts.app')
@section('title')
Create Article
@endsection
@section('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
    <link href="{{ asset('css/dropify.min.css') }}" rel="stylesheet">
@endsection
@section('content') 
    <div class="row" style="width:100%">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Change Photo <span style="float:right"><a href="/profile" class="btn btn-info btn-xs">Back</a></span></div>
                <div class="panel-body">

                    @if(session('success'))
                        <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                            {{ session('success') }}
                        </div>
                    @endif

                <form action="{{ route('change-image') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value=" {{ Auth::user()->id }} ">
            
                    <div class="form-group">
                    <label for="title" class="">Photo</label>
                        <div class="divider"></div>
                            <div class="row section">
                                <div class="col-sm-12 col-md-12 col-lg-12"><br>
                                        <input type="file" name="image" id="input-file-events" data-height="350" class="dropify-event" data-default-file="{{ Storage::url(Auth::user()->photo) }}" value="{{ Auth::user()->photo }}">
                                </div><br>
                            </div> 
                        @if ($errors->has('image'))
                            <p style="color:#bf0000">{{ $errors->first('image') }} </p>
                        @endif
                        </div>
                            
             

                    <input type="submit" value="submit" class="btn btn-success pull-right">
                </form>

                </div>
            </div>
        </div>
    </div>


@endsection   
@section('js')
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"><script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
      <script src="{{ asset('js/dropify.min.js') }}"></script>
     <script type="text/javascript">
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('.dropify-event').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.filename + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });
        });
    </script>
@endsection   
