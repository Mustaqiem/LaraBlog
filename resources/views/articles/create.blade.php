@extends('layouts.app')
@section('title')
Create Article
@endsection
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />    <link href="{{ asset('css/dropify.min.css') }}" rel="stylesheet">
@endsection
@section('content') 
    <div class="row" style="width:100%">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Article</div>
                <div class="panel-body">

                <form action="/articles" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value=" {{ Auth::user()->id }} ">
                
                    <div class="form-group">
                        <label for="title" class="">Title</label>
                        <input type="text" name="title" value="{{ old('title') }}" id="" placeholder="Title" class="form-control">
                        @if ($errors->has('title'))
                            <p style="color:#bf0000">{{ $errors->first('title') }} </p>
                        @endif
                    </div>

                    <div class="form-group">
                    <label for="title" class="">Thumbnail</label>
                        <div class="divider"></div>
                            <div class="row section">
                                <div class="col-sm-12 col-md-12 col-lg-12"><br>
                                        <input type="file" name="image" id="input-file-events" data-height="400" class="dropify-event" data-default-file="" value="{{ old('image') }}">
                                </div><br>
                            </div> 
                        @if ($errors->has('image'))
                            <p style="color:#bf0000">{{ $errors->first('image') }} </p>
                        @endif
                        </div>
                            
                    <div class="form-group">
                        <label for="content" class="">Content</label>
                        <textarea id="summernote" name="content" class="form-control" placeholder="Content">{{ old('content') }}</textarea>
                        @if ($errors->has('content'))
                            <p style="color:#bf0000">{{ $errors->first('content') }} </p>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="live" class="">
                            Live                                
                            <input type="checkbox" class="" name="live" id="">
                        </label>
                    </div>

                    

                    <input type="submit" value="submit" class="btn btn-success pull-right">
                </form>

                </div>
            </div>
        </div>
    </div>


@endsection   
@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').froalaEditor();
    });
  </script>
      <script src="{{ asset('js/dropify.min.js') }}"></script>
     <script type="text/javascript">
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('.dropify-event').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.filename + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });
        });
    </script>
@endsection   
