@extends('layouts.app')
<style>
    img.article-img {
        width: 100%;
        height: 150px;
    }

    a.suka {
        font-size:13px;
    }
    a.balas {
        font-size:13px;
    }
    
</style>
@section('title')
Article
@endsection
@section('content') 
<div class="container">
    <div class="row" style="width:100%">
        <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">My articles
                    <span class="pull-right">
                    </span>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row" style="width:100%">

        @forelse($articles as $article)
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        {{--  @if($article->user_id == Auth::id())
                        <form action="/articles/{{ $article->id }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button class="btn btn-default btn-xs"><i class="material-icons" style="font-size:20px;">backspace</i></button>
                            
                        </form>
                        @endif  --}}

                        <span>{{ $article->title }}</span>
                       
                        <span class="pull-right">{{ $article->created_at->diffForHumans() }}
                            
                        </span>
                    </div>
                    <div class="panel-body">
                    
                        <img src="{{ Storage::url($article->image) }}" alt="" class="article-img">
                        <p>{{ strip_tags($article->shortContent) }}</p>
                        <a href="/articles/{{ $article->slug }}">Readmore</a>
                    </div>

                    <div class="panel-footer clearfix" style="background:white">
                   <!-- <a href="#"><i class="material-icons pull-right">mood_bad</i></a> -->
                    <a style="text-decoration:none" href="#share" class="pull-right"><i class="material-icons" title="Delete article">backspace</i></a>
                    <center><a style="text-decoration:none" href="/articles/{{ $article->id }}/edit" title="Edit article"><i class="material-icons" style="font-size:20px">mode_edit&nbsp;</i></a>
                    <a style="text-decoration:none" href="/articles/{{ $article->slug }}" class="pull-left" title="See article"><i class="material-icons">info_outline</i></a>
                    </div>                            
                </div>
            </div>
        @empty
            <center>No articles.</center>
        @endforelse
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    {{$articles->links()}}
                </div>
            </div>

       
    </div>
@endsection   
