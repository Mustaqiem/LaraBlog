@extends('layouts.app')
@section('title')
Edit
@endsection
@section('css')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <link href="{{ asset('css/dropify.min.css') }}" rel="stylesheet">
@endsection
@section('content') 
    <div class="row" style="width:100%">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Article  <span style="float:right"><a href="/articles" class="btn btn-info btn-xs">Back</a></span></div>
                <div class="panel-body">

                    @if(session('success'))
                        <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                            {{ session('success') }}
                        </div>
                    @endif
                
                <form action="/articles/{{ $article->id }}" method="post" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value=" {{ Auth::user()->id }} ">

                    <div class="form-group">
                        <label for="title" class="">Title</label>
                        <input type="text" name="title" id="" value="{{ $article->title }}" placeholder="Title" class="form-control">
                    </div>

                    {{--  <div class="form-group">
                            <label for="title" class="">Picture</label>
                            <input type="file" name="image" id="input-file-events" data-height="400" class="dropify-event" data-default-file="{{ Storage::url($article->image) }}" value="{{$article->image}}">
                        </div>  --}}

                    <div class="form-group">
                        <label for="content" class="">Content</label>
                        <textarea name="content" id="summernote" class="form-control" placeholder="Content">{{ $article->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="live" class="">
                            Live                           
                            <input type="checkbox" class="" name="live" id="" {{ $article->live == 1 ? 'checked' : '' }}>
                        </label>
                    </div>

                    

                    <input type="submit" value="submit" class="btn btn-success pull-right">
                </form>

                </div>
            </div>
        </div>
    </div>


@endsection 
@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
      <script src="{{ asset('js/dropify.min.js') }}"></script>
     <script type="text/javascript">
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('.dropify-event').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.filename + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });
        });
    </script>
@endsection   
  