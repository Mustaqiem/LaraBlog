@extends('layouts.app')
@section('title')
{{$article->slug}}
@endsection
<style>
    img.article-img {
        width: 100%;
        height: 250px;
    }

    a.suka {
        font-size:13px;
    }
    a.balas {
        font-size:13px;
    }
    
</style>
@section('content') 
    <div class="row" style="width:100%">

            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Article by {{$article->user->name}}</span>
     
                        <span class="pull-right">{{ $article->created_at->diffForHumans() }}</span>
                    </div>

                    <div class="panel-body">
                        <img src="{{ Storage::url($article->image) }}" alt="" class="article-img">
                        
                        &emsp;&emsp; {!! $article->content !!}
                    </div>
                    <div class="panel-footer clearfix" style="background:white">
                   <!-- <a href="#"><i class="material-icons pull-right">mood_bad</i></a> -->
                    <a style="text-decoration:none" href="#share" class="pull-right"><i class="material-icons" title="Share">share</i>100</a>
                    <center><a style="text-decoration:none" href="#comment"><i class="material-icons comment" title="Comment">insert_comment</i>{{ $article->count_comment }}</a>
                    {{--  @if ($Auth::user()->id)  --}}
                        
                    {{--  @endif  --}}
                    <a style="text-decoration:none" href="/like/{{ $article->id }}" class="pull-left"><i class="material-icons" title="Like">thumb_up</i>{{ $article->count_like }}</a>
                    </div>
                    <div class="panel-footer clearfix dash-comment" style="background:white">
                        <button class="btn btn-default btn-sm btn-block">
                           lihat pesan sebelumnya
                         </button>
                    <div class="row" style="width:100%"><br> 
                    @foreach($article->comment as $comments)
                        <img class="col-md-8" src="{{ Storage::url($comments->user->photo) }}" style="width:70px;height:50px" alt="">
                        <b>{{ $comments->user->name }}</b> {{ $comments->comment }}  <br><br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i>{{$comments->created_at->diffForHumans()}}</i>&emsp;. <a href="/like/comment/{{$comments->id}}" class="suka">Suka</a>&emsp;|&emsp;<a href="#balas" class="balas">Balas</a>  
                        <a href="#">
                            <b class="pull-right" style="color:#3097d1"><i class="material-icons" style="font-size:16px">thumb_up</i>10&emsp;</b>
                        </a>
                        <br><br> 
                    @endforeach                         

                    </div>
                        <form action="{{route('comment.article', $article->id)}}" method="POST">
                            
                            {{ csrf_field() }}

                            <input type="hidden" name="user_id" value=" {{ Auth::user()->id }} ">

                            <br>
                            <textarea placeholder="comment" name="comment" class="form-control" id="" cols="50" rows="3"></textarea><br>
                            <button class="btn btn-primary">kirim</button>
                        </form>
                    </div>

                </div>
            </div>
    </div>
   
    @endsection   
@section('js') 
<script>
    $(Document).ready(function() {
        $('.comment').click(function() {
            $('.dash-comment').toggle(1000);
        });
    });
</script>  
@endsection   