@extends('layouts.app')
@section('title')
Dashboard
@endsection
<style>
    img.article-img {
        width: 100%;
        height: 250px;
    }

    a.suka {
        font-size:13px;
    }
    a.balas {
        font-size:13px;
    }
    

</style>
@section('content')

<div class="container">
    <div class="row" style="width:100%">
        <div class="col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard
                    <span class="pull-right">
                        {{--  <form action="" method="">
                            <input type="text" name="search" placeholder="search" class="form-control">
                        </form>  --}}
                    </span>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="width:100%">

        @forelse($articles as $article)
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    {{--  <div class="panel-heading">
                        <span><b>{{ $article->title }}</b></span>
                        <span class="pull-right">{{ $article->created_at->diffForHumans() }}
                        </span>
                    </div>  --}}

                    <div class="panel-body row">                        
                        <img src="{{ Storage::url($article->image) }}" style="width:250px;height:250px" alt="" class="article-img col-md-1">
                        <span class="col-md-7" style="left:10px;font-size:15px"><b><a class="title" href="/articles/{{ $article->id }}" style="text-decoration:none;">{{ $article->title }}</a></b></span>
                        <div class="row">
                        <a class="col-md-4" href="" style="left:10px;margin-top:40px;text-decoration:none">{{ $article->user->name }}</a>
                        <li class="col-md-3" style="margin-top:40px">{{ $article->created_at->diffForHumans() }}</li>
                        </div>
                        <div class="right col-md-7 col-md-offset-5" style="margin-top:-25%">
                        {{ strip_tags($article->shortContent) }}
                        <a href="/articles/{{ $article->slug }}">Readmore</a>
                        </div>
                    </div>

                    {{--  <div class="panel-footer clearfix" style="background:white">
                   <!-- <a href="#"><i class="material-icons pull-right">mood_bad</i></a> -->
                    <a style="text-decoration:none" href="/like/count/{{ $article->id }}" class="pull-right"><i class="material-icons" title="Share">share</i>100</a>
                    <center><a style="text-decoration:none" href="/articles/{{ $article->id }}"><i class="material-icons comment" title="Comment">insert_comment</i>50</a>
                    <a style="text-decoration:none" href="/like/{{ $article->id }}" class="pull-left"><i class="material-icons" title="Like">thumb_up</i>100</a>
                    </div>                              --}}
                </div>
            </div>
        @empty
            <center>No articles.</center>
        @endforelse
            <div class="row" style="width:100%">
                <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
                    {{$articles->links()}}
                </div>
            </div>

       
    </div>
@endsection
