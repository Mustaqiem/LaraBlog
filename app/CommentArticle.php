<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentArticle extends Model
{
    protected $fillable = [
        'user_id', 'article_id', 'comment'
    ];

    protected $table = 'comment_article';

    public function user()
    {
        return $this->belongsTo('App\User');        
    }
}
