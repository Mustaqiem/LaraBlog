<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use DB;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile()
    {
        // $user = User::whereUsername($username)->first();
       
        return view('user.profile');
    }

    public function getChangeImage()
    {
        return view('user.change-image');
    }

    public function postChangeImage(Request $request)
    {
        $user = Auth::user();
  
        if (!$request->file('image')) {
            $user->photo = $user->photo;
            $user->update();
        } else {
            $updloadFile = $request->file('image');
            $path = $updloadFile->store('public/images');
            $user->photo = $path;
            $user->update();
        }

        return redirect()->back()->withSuccess('Photo has been update');

    }
}
