<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Article;
use App\LikeArticle;
use App\CommentArticle;
use Auth;
use DB;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('user')->orderBy('created_at', 'desc')
        ->where('user_id', Auth::id())
        ->paginate(10);
        // $articles = Article::whereLive(1)->get();

        // using query builder
        // $articles = DB::table('articles')->get();
        // $articles = DB::table('articles')->get()->where('live', 1);
        // ->where('live', '1')

        // ->first();
        

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article;
    
        $this->validate($request, [
            'title'   => 'required',
            'image'   => 'required|image',
            'content' => 'required'
        ]);

        $updloadFile = $request->file('image');
        $path = $updloadFile->store('public/images');
        // $image = $file->move($path,$file->getClientOriginalName());
        
        $article::create([
            'user_id' => Auth::user()->id,
            'title'   => $request->title,
            'slug'    => str_slug($request->title) /**. "-" .rand(1, 15)**/,
            'image'   => $path,
            'content' => $request->content,
            'live'    => $request->live
        ]);

        // if ($request->hasFile('image')) {
        //     $request->file('image');
        //     return $image;
        // } else {
        //     return "No image selected";
        // }

       

        return redirect('/articles')->withSuccess('yes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $article = Article::with("user", "comment.user", "like.user")->where('slug', $slug)->first();
        // dd($article);die;
        // $like = LikeArticle::where('article_id', $article->id)
        // ->get();\CreateUsersTable

        $article->count_comment = count($article->comment);
        $article->count_like = count($article->like);

        // dd($article->count_comment);die;

        // $comment = CommentArticle::with("user")->orderBy('created_at', 'asc')
        // ->where('article_id', $article->id)->get()
        // ;
        // dd($comment);die;
        // var_dump(count($commentArticle));die;
        // dd($commentArticle);
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articles = new Article;
        $article = Article::findOrFail($id);

        if (! isset($request->live))
                $article->update(array_merge($request->all(), ['live' == false]));
        else 
        // $updloadFile = $request->file('image');
        // $path = $updloadFile->store('public/images');
        // $image = $file->move($path,$file->getClientOriginalName());
        // dd($article->image);die;
        $article->title = $request->title;
        $article->slug = str_slug($article->title);
        $article->image = $article->image;
        $article->content = $request->content;
        $article->live = $request->live;
        $article->update();
    
        return redirect()
        ->back()
        ->withSuccess('Article has been update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $article = Article::findOrFail($id);
        // $article->delete();
        Article::destroy($id);

        return redirect('/articles');
    }
}
