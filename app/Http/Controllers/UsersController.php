<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;

class UsersController extends Controller
{
    public function getChangePassword()
    {
        return view('user.change-password');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->withError('Your current password does not matches with the password you provided. Please try again.');            
        } 

        $this->validate($request, [
            'current-password' => 'required',
            'password'         => 'required|min:6|max:20|confirmed'
        ]);

        // Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->update();

        return redirect()->back()->withSuccess('Password has been update');
    }

    public function seeUser($id)
    {

    }
}
