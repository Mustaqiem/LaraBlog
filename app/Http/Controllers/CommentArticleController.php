<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CommentArticle;
use App\Article;
use App\LikeComment;
use Auth;
use DB;

class CommentArticleController extends Controller
{
    public function createCommentArticle(Request $request, $id)
    {
        $article = new Article;
        $findArticle = Article::findOrFail($id);
        $commentArticle = new CommentArticle;

        $this->validate($request, [
            'comment'   => 'required',
        ]);

       $commentArticle::create([
            'user_id' => Auth::id(),
            'article_id' => $findArticle->id,
            'comment' => $request->comment
        ]);

    //    dd($tes);die;        

        return back();
    }

    public function countCommentArticle($id)
    {
        $article = new Article;
        $findArticle = Article::findOrFail($id);
        $commentArticle = new CommentArticle;

        $commentArticle = CommentArticle::where('user_id', Auth::id())
        ->where('article_id', $findArticle->id)
        ->get();
        
        
        return count($commentArticle);
    }

    public function getAllCommentArticleById($id)
    {
        $findArticle = Article::findOrFail($id);
       
        $comment = CommentArticle::orderBy('created_at', 'desc')
        ->where('article_id', $findArticle->id)
        ->paginate(10);

        // dd($comment);
        
        return view('articles.show', compact('comment'));
    }

    public function setLikeComment($id)
    {
        // $c = new LikeComment;
        $comment = CommentArticle::where('id', $id)->first();

        LikeComment::create([
            'user_id' => Auth::user()->id,
            'comment_id' => $comment->id,
            'like'      => 'like'
        ]);

        return redirect()->back();

    }
}
