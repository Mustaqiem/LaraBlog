<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\LikeArticle;
use App\LikeComment;
use Auth;
use DB;

class LikeArticleController extends Controller
{
    public function countLikeArticle($id)
    {
        $article = new Article;        
        $findArticle = Article::findOrFail($id);

        $like = LikeArticle::where('user_id', Auth::id())
        ->where('article_id', $findArticle->id)
        ->get();


        // return view('home', compact('count'));
        
        return count($like);
    }

    public function setLikeArticle($id)
    {
        $like = new LikeArticle;
        $article = new Article;        
        $findArticle = Article::findOrFail($id);
        // dd($findArticle->id);die;
        $like::create([
            'user_id' => Auth::id(),
            'article_id' => $findArticle->id,
            'like' => 'like',
        ]);
 
        return redirect()->route('articles.show', ['id' => $findArticle]);
    }

    public function setUnlikeArticle($id)
    {
        $findArticle = Article::findOrFail($id);
        $like = new LikeArticle;
        
        $like->update([
            'user_id' => Auth::id(),
            'article_id' => $findArticle->id,
            'like' => 'unlike',
        ]);
    }

}
