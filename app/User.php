<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'dob', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'dob'
    ];


    // Laravel Mutators

    public function setNameAttribute($val)
    {
         $this->attributes['name'] = ucfirst($val);
    }

    public function setPasswordAtrribute($val)
    {
         $this->attributes['password'] = bcrypt($val);
    }

    // Laravel Accessors

    // public function getNameAttribute($value)
    // {
    //     return strtoupper($value);
    // }

    public function getEmailAttribute($value)
    {
        return strtok( $value, '@');
    }

}
