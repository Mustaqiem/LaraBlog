<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeArticle extends Model
{
    protected $fillable = [
        'user_id', 'article_id', 'like'
    ];

    protected $table = 'like_article';

    public function user()
    {
        return $this->belongsTo('App\User');        
    }
}
