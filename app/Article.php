<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // protected $table = 'articles';

    protected $fillable = [
        'user_id', 'content', 'live', 'title', 'image', 'slug'
    ];

    protected $dates = ['post_on', 'deleted_at'];

    // protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User');        
    }

    public function comment()
    {
        return $this->hasMany('App\CommentArticle');        
    }

    public function like()
    {
        return $this->hasMany('App\LikeArticle');        
    }

    public function setLiveAttribute($value)
    {
        $this->attributes['live'] = (boolean)($value);
    }

    public function getShortContentAttribute()
    {
    //    return substr($this->content, 0, random_int(60, 150)). '...';
       return substr($this->content, 0,  200). ' . . . ';
    }

    
}
