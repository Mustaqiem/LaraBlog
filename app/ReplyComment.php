<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $comment_id
 * @property int $user_id
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 * @property CommentArticle $commentArticle
 * @property User $user
 */
class ReplyComment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['comment_id', 'user_id', 'comment', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commentArticle()
    {
        return $this->belongsTo('App\CommentArticle', 'comment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
